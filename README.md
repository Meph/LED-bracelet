# LED-bracelet

Templates for Stiching bracelet with LEDs.
Use this one to create your own design.
I recommend to use Incscape and the svg files.
The scale is 1:1.

# LED Armband

Vorlage für Stickarmband mit LEDs.
Nutze diese Vorlage für eigene Designs.
Ich empfehle die svg-Dateien mit Inkscape zu bearbeiten.
Der Maßstab ist 1:1.

## How to build - Bastelanleitungen
Images and explanations how to build the bracelets in German.<br>
Bilder und Erklärungen, wie es gebastelt werden kann.

RGB-LEDs: https://nerdculture.de/@M/107733493874488230 <br>
Pumpkin / Kürbis: https://nerdculture.de/@M/107150089771674667 <br>
Bat / Fledermaus: https://nerdculture.de/@M/107333226493681359 <br>
Ghost / Gespenst: https://nerdculture.de/@M/107333548329825760 <br>

<br /><br /><br />
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
<br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz</a>.
<br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.